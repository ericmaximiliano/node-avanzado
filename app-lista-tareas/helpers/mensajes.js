const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

const pausa = async() => {
  //  return new Promise(resolve =>{
         rl.question('PRESIONE ENTER PARA CONTINUAR', (enter) => {
         rl.close()
  //       resolve();
        })
    //})
}

const mostrarMenu = () => {
    return new Promise (resolve => {
        console.log(`${'====================='.yellow}`)
        console.log(`${'Seleccione una opcion'.blue}`)
        console.log(`${'====================='.yellow}`)   
    
        console.log(`${'¿Que desea hacer?'.red}`)   
        console.log(`${'1.'.rainbow} Crear tabla`)
        console.log(`${'2.'.rainbow} Listar tareas`)
        console.log(`${'3.'.rainbow} Listar tareas completadas`)
        console.log(`${'4.'.rainbow} Listar tareas pendientes`)
        console.log(`${'5.'.rainbow} Completar tarea(s)`)
        console.log(`${'6.'.rainbow} Borrar tarea`)
        console.log(`${'0.'.rainbow} Salir`)
        rl.question('Seleccione una opcion:', (opcion) => {
            console.log("Opcion: ", opcion);
            rl.close()
            resolve(opcion);
        })
    })
}

module.exports = {
    mostrarMenu,
    pausa
}