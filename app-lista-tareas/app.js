// modulo: node avanzado.
console.clear()
const color = require("colors")
const {pausa, mostrarMenu} = require('./helpers/mensajes');
const main = async() => {
    await mostrarMenu();
    await pausa();
}
main();
